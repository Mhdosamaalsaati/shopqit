<?php

use App\Http\Controllers\BackeHomeController;
use App\Http\Controllers\Back\CategoryController;
use App\Http\Controllers\Back\ProductController;
use App\Http\Controllers\RolesController;
use App\Http\Controllers\WebSiteController;
use Illuminate\Support\Facades\Route;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

// $super_admin = false;
// $roles = Role::get();
// $count_roles = count($roles);

// if ($count_roles == 0) {
//     Route::get('/', function () {
//         return view('sedder');
//     });
// } else {
//     Route::get('/', function () {
//         return view('welcome');
//     });
// }

// Route::post('/create-sedder', function () {
//     $role = Role::create([
//         'name' => 'super_admin',
//     ]);

//     $permissions = [
//         'role',
//         'product',
//         'category',
//         'sub_category',
//     ];

//     foreach ($permissions as $key => $permission) {
//         $permission = Permission::create(['name' => $permission]);
//         $permission->assignRole($role);
//     }
// })->name('sedder');

Route::prefix('back')->name('back.')->group(function () {
    Route::get('/', BackeHomeController::class)->middleware('auth')->name('index');

    Route::controller(CategoryController::class)->middleware('auth')->group(function () {
        Route::get('/allcategory', 'index')->name('allcategory');
        Route::get('/addcategory', 'Addcategory')->name('addcategory');
        Route::post('/store-category', 'storecategory')->name('storecategory');
        Route::get('/edit-category/{id}', 'editcategory')->name('editcategory');
        Route::post('/update-category/{id}', 'updatecategory')->name('updatecategory');
        Route::get('/delete-category/{id}', 'deletecategory')->name('deletecategory');

    });


    Route::controller(ProductController::class)->middleware('auth')->group(function () {
        Route::get('/all-product', 'index')->name('allproduct');
        Route::get('/add-product', 'Addproduct')->name('addproduct');
        Route::post('/store-product', 'storeproduct')->name('storeproduct');
        Route::get('/edit-product/{id}', 'editproduct')->name('editproduct');
        Route::post('/update-product/{id}', 'updateproduct')->name('updateproduct');
        Route::get('/delete-product/{id}', 'deleteproduct')->name('deleteproduct');
    });

    Route::controller(RolesController::class)->group(function () {
        Route::get('all-roles', 'index')->name('roles.index');
        Route::get('roles/new/create', 'create')->name('roles.create');
        Route::post('roles/new/store', 'store')->name('roles.store');
    });
});
require __DIR__ . '/auth.php';

Route::controller(WebSiteController::class)->group(function(){
    Route::get('/', 'home')->name('home');
    Route::get('/category/{id}','category')->name('category');
    Route::get('/product-detials/{id}','singleproduct')->name('singleproduct');
});
// Route::get('/', function () {
//           return view('welcome');
//          });
