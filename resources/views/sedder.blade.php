<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Install Sedder with project</title>
</head>
<body>
    <h1>Sedder</h1>
    <form action="{{ route('sedder') }}" method="post">
        @csrf
        <button class="btn btn-primary">Create Seeder To Setup Project</button>
    </form>
</body>
</html>
