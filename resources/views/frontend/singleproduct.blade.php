@extends('frontend.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-sm-4">
                <div class="box_main">
                    <div class="tshirt_img"><img src="{{ asset($singleproduct->images->image ?? '') }}">
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-4">
                <div class="box_main">
                    <div class="product-info">
                        <h4 class="shirt_text text-left">{{ $singleproduct->product_name }}</h4>
                        <p class="price_text text-left">Price <span
                                style="color: #262626;">${{ $singleproduct->price }}</span></p>
                    </div>
                    <div class="my-3 product-details">
                        <p class="lead">Descriptions {{ $singleproduct->product_des }}</p>
                        <ul class="p-2 bg-light my-2">
                            <li>{{ $singleproduct->category_name }}</li>
                        </ul>
                    </div>
                    <div class="btn_main">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="fashion_section">
        <div id="main_slider">
            <div class="container">
                <h1 class="fashion_taital">Related Product</h1>
                <div class="fashion_section_2">
                    <div class="row">
                        @foreach ($allproduct as $product)
                            <div class="col-lg-4 col-sm-4">
                                <div class="box_main">
                                    <h4 class="shirt_text">{{ $product->product_name }}</h4>
                                    <p class="price_text">Price <span style="color: #262626;">$ {{ $product->price }}</span>
                                    </p>
                                    <div class="tshirt_img"><img src="{{ asset($singleproduct->images->image ?? '') }}">
                                    </div>
                                    <div class="btn_main">
                                        <div class="buy_bt">
                                        </div>
                                        <div class="seemore_bt"><a href="{{ route('singleproduct', [$product->id]) }}">See
                                                More</a></div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a class="carousel-control-prev" href="#main_slider" role="button" data-slide="prev">
        <i class="fa fa-angle-left"></i>
    </a>
    <a class="carousel-control-next" href="#main_slider" role="button" data-slide="next">
        <i class="fa fa-angle-right"></i>
    </a>
@endsection
