<!DOCTYPE html>
<html lang="en">
  <head>
   @include('frontend.css')
   @yield('style')
  </head>
  <body>
      @include('frontend.navbar')
        <!-- partial -->
      @yield('content')
          <!-- partial -->

      @include('frontend.footer')

    @include('frontend.script')
    @yield('script')
    <!-- End custom js for this page -->
  </body>
</html>
