@php
    $category = App\Models\Category::latest()->get();
@endphp
<div class="banner_bg_main">
    <!-- header top section start -->
    <div class="container">
        <div class="header_section_top">
            <div class="row">
                <div class="col-sm-12">
                    <div class="custom_menu">
                        <ul>
                            <li><a href="{{ route('home') }}">Best Sellers</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- header top section start -->
    <!-- logo section start -->
    <div class="logo_section">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="logo"><a href="index.html"><img
                                src="{{ asset('assets-frontend') }}/images/logo.png"></a></div>
                </div>
            </div>
        </div>
    </div>
    <div class="header_section">
        <div class="container">
            <div class="containt_main">
                <div id="mySidenav" class="sidenav">
                    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                    <a href="{{ route('home') }}">Home</a>
                    @foreach ($category as $categories)
                        <a href="{{ route('category', [$categories->id]) }}">{{ $categories->category_name }}</a>
                    @endforeach
                </div>
                <span class="toggle_icon" onclick="openNav()"><img
                        src="{{ asset('assets-frontend') }}/images/toggle-icon.png"></span>
                <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        All Category
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        @foreach ($category as $categories)
                            <a class="dropdown-item"
                                href="{{ route('category', [$categories->id]) }}">{{ $categories->category_name }}</a>
                        @endforeach
                    </div>
                </div>
                <div class="main">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search this blog">
                        <div class="input-group-append">
                            <button class="btn btn-secondary" type="button"
                                style="background-color: #f26522; border-color:#f26522 ">
                                <i class="fa fa-search"></i>
                            </button>
                        </div>
                    </div>

                </div>
                <div class="header_box">
                    <div class="lang_box">
                        @if (Route::has('login'))
                            <a href="#" title="User" class="nav-link" data-toggle="dropdown"
                                aria-expanded="true">
                                <i class="fa fa-user mr-2" aria-hidden="true"></i>
                                @auth
                                    <span style="color: black;">{{ Auth::user()->name }}</span>
                                @else
                                    <span style="color: black;">Log in</span>
                                @endauth
                                <i class="fa fa-angle-down ml-2" aria-hidden="true"></i>
                            </a>
                            <div class="dropdown-menu">
                                @auth
                                    <form action="{{ route('logout') }}" method="POST" class="dropdown-item">
                                        @csrf
                                        <button type="submit">
                                            <i class="fa fa-power-off mr-2" aria-hidden="true"></i>
                                            <span style="color: black;">Log Out</span>
                                        </button>
                                    </form>
                                @else
                                    <a href="{{ route('login') }}" class="dropdown-item">
                                        <i class="fa fa-sign-in mr-2" aria-hidden="true"></i>
                                        <span style="color: black;">Log in</span>
                                    </a>

                                    @if (Route::has('register'))
                                        <a href="{{ route('register') }}" class="dropdown-item">
                                            <i class="fa fa-user-plus mr-2" aria-hidden="true"></i>
                                            <span style="color: black;">Register</span>
                                        </a>
                                    @endif
                                @endauth
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- header section end -->
    <!-- banner section start -->
    <div class="banner_section layout_padding">
        <div class="container">
            <div id="my_slider" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <div class="row">
                            <div class="col-sm-12">
                                <h1 class="banner_taital">Get Start <br>Your favriot shoping</h1>
                                <div class="buynow_bt"><a href="{{ route('home') }}">Buy Now</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="row">
                            <div class="col-sm-12">
                                <h1 class="banner_taital">Get Start <br>Your favriot shoping</h1>
                                <div class="buynow_bt"><a href="#">Buy Now</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="row">
                            <div class="col-sm-12">
                                <h1 class="banner_taital">Get Start <br>Your favriot shoping</h1>
                                <div class="buynow_bt"><a href="#">Buy Now</a></div>
                            </div>
                        </div>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#my_slider" role="button" data-slide="prev">
                    <i class="fa fa-angle-left"></i>
                </a>
                <a class="carousel-control-next" href="#my_slider" role="button" data-slide="next">
                    <i class="fa fa-angle-right"></i>
                </a>
            </div>
        </div>
    </div>
    <!-- banner section end -->
</div>
