@extends('frontend.app')

@section('content')
   <body>
      <!-- header section start -->
      <div class="header_section haeder_main">
         <div class="container-fluid">
            <nav class="navbar navbar-light bg-light justify-content-between">
               <div id="mySidenav" class="sidenav">
                  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                  <a href="index.html">Home</a>
                  <a href="computers.html">Computers</a>
                  <a href="mans_clothes.html">Mans Clothes</a>
                  <a href="womans_clothes.html">Womans Clothes</a>
                  <a href="{{ route('contact') }}">Contact</a>
               </div>
               <span style="font-size:30px;cursor:pointer; color: #fff;" onclick="openNav()"><img src="{{ asset('assets-frontend') }}/images/toggle-icon.png"></span>
               <a class="navbar-brand" href="index.html"><img src="{{ asset('assets-frontend') }}/images/logo.png"></a></a>
               <form class="form-inline ">
                  <div class="login_text">
                     <ul>
                        <li><a href="#"><img src="{{ asset('assets-frontend') }}/images/user-icon.png"></a></li>
                        <li><a href="#"><img src="{{ asset('assets-frontend') }}/images/trolly-icon.png"></a></li>
                        <li><a href="#"><img src="{{ asset('assets-frontend') }}/images/search-icon.png"></a></li>
                     </ul>
                  </div>
               </form>
            </nav>
         </div>
      </div>
      <!-- header section end -->
      <!-- contact section start -->
      <div class="contact_section layout_padding">
         <div class="container">
            <h1 class="contact_taital">Contact Us</h1>
            <div class="contact_section_2">
               <div class="mail_section_1">
               <input type="text" class="mail_text" placeholder="Name" name="text">
               <input type="text" class="mail_text" placeholder="Email" name="text">
               <input type="text" class="mail_text" placeholder="Phone Number" name="text">
               <textarea class="massage-bt" placeholder="Massage" rows="5" id="comment" name="Massage"></textarea>
               <div class="send_bt"><a href="#">Send</a></div>
               </div>
            </div>
         </div>
      </div>

@endsection
