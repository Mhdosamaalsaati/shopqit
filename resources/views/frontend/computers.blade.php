@extends('frontend.app')

@section('content')

      <title>Computers</title>

      <!-- header section start -->
      <div class="header_section haeder_main">
         <div class="container-fluid">
            <nav class="navbar navbar-light bg-light justify-content-between">
               <div id="mySidenav" class="sidenav">
                  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                  <a href="">Home</a>
                  <a href="">Computers</a>
                  <a href="">Mans Clothes</a>
                  <a href="">Womans Clothes</a>
                  <a href="">Contact</a>
               </div>
               <span style="font-size:30px;cursor:pointer; color: #fff;" onclick="openNav()"><img src="{{ asset('assets-frontend') }}/images/toggle-icon.png"></span>
               <a class="navbar-brand" href="index.html"><img src="{{ asset('assets-frontend') }}/images/logo.png"></a></a>
               <form class="form-inline ">
                  <div class="login_text">
                     <ul>
                        <li><a href="#"><img src="{{ asset('assets-frontend') }}/images/user-icon.png"></a></li>
                        <li><a href="#"><img src="{{ asset('assets-frontend') }}/images/trolly-icon.png"></a></li>
                        <li><a href="#"><img src="{{ asset('assets-frontend') }}/images/search-icon.png"></a></li>
                     </ul>
                  </div>
               </form>
            </nav>
         </div>
      </div>
      <!-- header section end -->
      <!-- computers section start -->
      <div class="computers_section layout_padding">
         <div class="container">
            <h1 class="computers_taital">Computers & Laptop</h1>
         </div>
      </div>
      <div class="computers_section_2">
         <div class="container-fluid">
            <div class="computer_main">
               <div class="row">
                  <div class="col-md-4">
                     <div class="computer_img"><img src="{{ asset('assets-frontend') }}/images/computer-img.png"></div>
                     <h4 class="computer_text">COMPUTER</h4>
                     <div class="computer_text_main">
                        <h4 class="dell_text">Samsung</h4>
                        <h6 class="price_text"><a href="#">$500</a></h6>
                        <h6 class="price_text_1"><a href="#">$1000</a></h6>
                     </div>
                     <div class="cart_bt_1"><a href="#">Add To Cart</a></div>
                  </div>
                  <div class="col-md-4">
                     <div class="computer_img"><img src="{{ asset('assets-frontend') }}/images/laptop-img.png"></div>
                     <h4 class="computer_text">LAPTOP</h4>
                     <div class="computer_text_main">
                        <h4 class="dell_text">Dell</h4>
                        <h6 class="price_text"><a href="#">$500</a></h6>
                        <h6 class="price_text_1"><a href="#">$1000</a></h6>
                     </div>
                     <div class="cart_bt_1"><a href="#">Add To Cart</a></div>
                  </div>
                  <div class="col-md-4">
                     <div class="computer_img"><img src="{{ asset('assets-frontend') }}/images/mac-img.png"></div>
                     <h4 class="computer_text">macOS</h4>
                     <div class="computer_text_main">
                        <h4 class="dell_text">Apple</h4>
                        <h6 class="price_text"><a href="#">$500</a></h6>
                        <h6 class="price_text_1"><a href="#">$1000</a></h6>
                     </div>
                     <div class="cart_bt_1"><a href="#">Add To Cart</a></div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      @endsection
