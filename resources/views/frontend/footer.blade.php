      <!-- jewellery  section end -->
      <!-- footer section start -->
      <div class="footer_section layout_padding">
        <div class="container">
           <div class="footer_logo"><a href="index.html"><img src="{{ asset('assets-frontend') }}/images/footer-logo.png"></a></div>
           <div class="input_bt">
              <input type="text" class="mail_bt" placeholder="Your Email" name="Your Email" >
              <p></p>
              <a class="copyright_text">Mhdosamaalsaati@gmail.com</a>
           </div>
           <div class="footer_menu">
              <ul>
                 <li><a href="{{ route('home') }}">Best Sellers</a></li>
                 <li><a href="#">Gift Ideas</a></li>
                 <li><a href="#">New Releases</a></li>
                 <li><a href="#">Today's Deals</a></li>
                 <li><a href="#">Customer Service</a></li>
              </ul>
           </div>
           <div class="location_main">Help Line  Number : <a href="#">+963968380509</a></div>
        </div>
     </div>
     <!-- footer section end -->
     <!-- copyright section start -->
     <div class="copyright_section">
        <div class="container">
           <p class="copyright_text">© 2023 All Rights Reserved. Programming by <a href="https://html.design">End.Osama Alsaati</a></p>
        </div>
     </div>

