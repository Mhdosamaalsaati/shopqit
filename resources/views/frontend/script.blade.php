<script src="{{ asset('assets-frontend') }}/js/jquery.min.js"></script>
<script src="{{ asset('assets-frontend') }}/js/popper.min.js"></script>
<script src="{{ asset('assets-frontend') }}/js/bootstrap.bundle.min.js"></script>
<script src="{{ asset('assets-frontend') }}/js/jquery-3.0.0.min.js"></script>
<script src="{{ asset('assets-frontend') }}/js/plugin.js"></script>
<!-- sidebar -->
<script src="{{ asset('assets-frontend') }}/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="{{ asset('assets-frontend') }}/js/custom.js"></script>
<script>
    function openNav() {
        document.getElementById("mySidenav").style.width = "250px";
    }

    function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
    }
</script>
