<!DOCTYPE html>

<html lang="en" class="light-style layout-menu-fixed" dir="ltr" data-theme="theme-default"
    data-assets-path="{{ asset('assets-back') }}/" data-template="vertical-menu-template-free">

@include('back.partials.head')

@livewireStyles

<body>
        
    @livewireScripts

    <!-- Layout wrapper -->
    <div class="layout-wrapper layout-content-navbar">
        <div class="layout-container">

            @include('back.partials.sidebar')

            <!-- Layout container -->
            <div class="layout-page">
                @include('back.partials.header')

                <!-- Content wrapper -->
                <div class="content-wrapper">
                    <!-- Content -->

                    <div class="container-xxl flex-grow-1 container-p-y">
                        @yield('content')
                    </div>
                    <!-- / Content -->

                    @include('back.partials.footer')


                    <div class="content-backdrop fade"></div>
                </div>
                <!-- Content wrapper -->
            </div>
            <!-- / Layout page -->
        </div>

        <!-- Overlay -->
        <div class="layout-overlay layout-menu-toggle"></div>
    </div>
    <!-- / Layout wrapper -->


    @include('back.partials.scripts')
    @if (session()->has('success'))
    <script>
        toastr.success("{{ session()->get('success') }}", 'Success');
    </script>
    @elseif (session()->has('error'))
    <script>
        toastr.error("{{ session()->get('error') }}", 'Erorr!');
    </script>
    @endif
</body>

</html>
