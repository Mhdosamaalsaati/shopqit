 <!-- Core JS -->
 <!-- build:js assets/vendor/js/core.js -->
 <script src="{{ asset('assets-back') }}/vendor/libs/jquery/jquery.js"></script>
 <script src="{{ asset('assets-back') }}/vendor/libs/popper/popper.js"></script>
 <script src="{{ asset('assets-back') }}/vendor/js/bootstrap.js"></script>
 <script src="{{ asset('assets-back') }}/vendor/libs/perfect-scrollbar/perfect-scrollbar.js"></script>

 <script src="{{ asset('assets-back') }}/vendor/js/menu.js"></script>
 <!-- endbuild -->

 <!-- Vendors JS -->
 <script src="{{ asset('assets-back') }}/vendor/libs/apex-charts/apexcharts.js"></script>

 <!-- Main JS -->
 <script src="{{ asset('assets-back') }}/js/main.js"></script>

 <!-- Page JS -->
 <script src="{{ asset('assets-back') }}/js/dashboards-analytics.js"></script>

 <!-- Place this tag in your head or just before your close body tag. -->
 <script async defer src="https://buttons.github.io/buttons.js"></script>

 <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

 <script>
     toastr.options.closeButton = true;
     toastr.options.closeMethod = 'fadeOut';
     toastr.options.closeDuration = 300;
     toastr.options.closeEasing = 'swing';
     toastr.options.progressBar = true;

 </script>
