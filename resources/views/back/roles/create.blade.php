@extends('back.master')
@section('title', 'Add Role')
{{-- @section('home_active', 'active') --}}

@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Page/</span> Add Role </h4>
        <div class="col-xxl">
            <div class="card mb-4">
                <div class="card-header d-flex align-items-center justify-content-between">
                    <h5 class="mb-0">Add New Role</h5>
                    <small class="text-muted float-end">Input information</small>
                </div>
                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form action="{{ route('back.roles.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf

                        <div class="row mb-3">
                            <label class="col-sm-2 col-form-label" for="basic-default-name">Role Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="role" name="role" />
                            </div>
                        </div>

                        <h5 class="mb-0">Permissions</h5>

                            <div class="col-sm-10">
                                @foreach ($permissions as $permission)
                                    <div class="form-check form-switch">
                                        <label class="form-check-label"
                                        for="permission{{ $permission->id }}">{{ $permission->name }}</label>

                                        <input class="form-check-input" id="flexSwitchCheckDefault"
                                         type="checkbox" name="permission[]" id="permission{{ $permission->id }}"
                                            value="{{ $permission->name }}">
                                    </div>
                                @endforeach
                            </div>

                        <div class="row justify-content-end">
                            <div class="col-sm-10">
                                <button type="submit" class="btn btn-primary">Add Role</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
