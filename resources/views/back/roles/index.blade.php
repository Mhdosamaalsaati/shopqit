@extends('back.master')
@section('title', 'All Roles')

@section('content')

<div class="container-xxl flex-grow-1 container-p-y">
    <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Page/</span> All Roles </h4>
    <div class="card">
        <h5 class="card-header">Available Roles Information</h5>
        <div class="table-responsive text-nowrap">
            <table class="table">
                <thead class="table-light">
                    <tr>
                        <th>ID</th>
                        <th>Role Name</th>
                      
                    </tr>
                </thead>
                <tbody class="table-border-bottom-0">
                    @foreach ( $roles as $role )

                    <tr>
                        <td>{{ $role->id }}</td>
                        <td>{{ $role->name }}</td>

                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection
