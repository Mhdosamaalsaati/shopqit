@extends('back.master')
@section('title', 'Add Product')
{{-- @section('home_active', 'active') --}}

@section('content')
<div class="container-xxl flex-grow-1 container-p-y">
    <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Page/</span> Add Product  </h4>
    <div class="col-xxl">
        <div class="card mb-4">
          <div class="card-header d-flex align-items-center justify-content-between">
            <h5 class="mb-0">Add New Product</h5>
            <small class="text-muted float-end">Input information</small>
          </div>
          <div class="card-body">
            @if ($errors->any())
            <div class="alert alert-danger">
           <ul>
           @foreach ($errors->all() as $error)
               <li>{{ $error }}</li>
           @endforeach
           </ul>
           </div>
           @endif
            <form action="{{ route('back.storeproduct') }}" method="POST" enctype="multipart/form-data">
                @csrf

              <div class="row mb-3">
                <label class="col-sm-2 col-form-label" for="basic-default-name">Product Name </label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="product_name" name="product_name"
                  placeholder="Electronics" />
                </div>
              </div>

              <div class="row mb-3">
                <label class="col-sm-2 col-form-label" for="basic-default-name">Product Price </label>
                <div class="col-sm-10">
                  <input type="number" class="form-control" id="price" name="price"
                  placeholder="12" />
                </div>
              </div>

              <div class="row mb-3">
                <label class="col-sm-2 col-form-label" for="basic-default-name">Product  Descriptions </label>
                <div class="col-sm-10">
                    <textarea class="form-control" name="product_des" id="product_des" cols="30" rows="10"></textarea>
                </div>
              </div>

              <div class="row mb-3">
                <label class="col-sm-2 col-form-label" for="category_id">Select Category</label>
                <div class="col-sm-10">
                    <select class="form-select" id="category_id" name="category_id" aria-label="Default select example">
                        <option selected>Open this select menu</option>
                        @foreach ( $categories as $category )
                        <option value="{{ $category->id }}">{{ $category->category_name }}</option>
                        @endforeach
                      </select>
                </div>
              </div>

              <div class="row mb-3">
                <label class="col-sm-2 col-form-label" for="basic-default-name">Uploade Product Image </label>
                <div class="col-sm-10">
                    <input class="form-control" type="file" id="image" name="image" />
                </div>
              </div>

              <div class="row justify-content-end">
                <div class="col-sm-10">
                  <button type="submit" class="btn btn-primary">Add Product</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
 </div>
@endsection
