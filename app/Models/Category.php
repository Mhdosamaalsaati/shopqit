<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;
    public $table = 'categories';

    protected $fillable = [
        'category_name',
        'parent_id',
        // 'subcatgory_count',
        // 'product_count',
    ];

    public function images() {
        return $this->morphOne(Image::class, 'imageable');
    }

    public function subcategories()
    {
        return $this->hasMany(Category::class, 'parent_id');
    }

    public function scopeWithoutSubcategories($query)
    {
        return $query->whereNull('parent_id')->doesntHave('subcategories');
    }

    public function products()
    {
        return $this->hasMany(Product::class, 'category_id', 'id');
    }



}
