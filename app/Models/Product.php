<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    public $table = 'products';

    protected $fillable = [
        'product_name',
        'product_des',
        'price',
        'category_id',
        'image',
    ];

    public function images() {
        return $this->morphOne(Image::class, 'imageable');
    }

    public function category()
    {
       return $this->belongsTo(Category::class, 'category_id','id');
    }
}
