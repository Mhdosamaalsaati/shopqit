<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    use HasFactory;
    public $table = 'images';

    public function category() {
        return $this->morphTo(Category::class);
    }

    public function product() {
        return $this->morphTo(Product::class);
    }
}
