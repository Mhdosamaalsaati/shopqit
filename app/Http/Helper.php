<?php

use App\Models\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

// Check if the save_image function does not exist, then define it
if(!function_exists('save_image')) {
    function save_image($file, $model, $model_id, Request $request) {
        $ext = $request->file($file)->getClientOriginalExtension();
        $path = "images/";
        $name = time().'.'.$ext;
        $request->file($file)->move(public_path($path), $name);
        $image = new Image();
        $image->image = $path.$name;
        $image->imageable_type = $model;
        $image->imageable_type = $model;
        $image->imageable_id = $model_id;
        $image->save();
        return $image->name;
    }
}

// Check if the checkPermission function does not exist, then define it
if(!function_exists('checkPermission')) {
    function checkPermission($permission) {
        return Auth::guard('web')->user()->can($permission);
    }
}
