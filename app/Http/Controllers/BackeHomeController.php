<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class BackeHomeController extends Controller
{
    public function __invoke(Request $request)
    {
        return view('back.home');
    }
}
