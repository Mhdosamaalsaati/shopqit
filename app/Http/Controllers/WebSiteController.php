<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;


class WebSiteController extends Controller
{

     public function home(){
         $allproduct = Product::latest()->get();
        return view('frontend.index',compact('allproduct'));
    }

    public function contact(){

        return view('frontend.contact');
    }

    public function category($id){
        $category=Category::findOrFail($id);
        $allproduct =Product::where('category_id',$id)->latest()->get();

        return view('frontend.category',compact('category','allproduct'));
    }

    public function singleproduct($id){
        $singleproduct =Product::findOrFail($id);
        $allproduct = Product::latest()->get();
        return view('frontend.singleproduct',compact('singleproduct','allproduct'));
    }

}
