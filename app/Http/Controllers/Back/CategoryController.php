<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class CategoryController extends Controller
{
    // Constructor with middleware to check user permissions
    public function __construct(){
        $this->middleware('can:category');
    }

    // Display all categories
    public function index(){
        // get all categories with their associated images, ordered by the latest
        $categories = Category::with(['images'])->latest()->get();
        return view('back.allcategory', compact('categories'));
    }

    public function Addcategory(){
        // get all categories from the model
        $category_info = Category::all();
        return view('back.addcategory', compact('category_info'));
    }

    // Store a new category in the database
    public function storecategory(Request $request){
        try {
            // Validate the incoming request
            $this->validate($request, [
                'image' => 'required',
                'category_name' => 'required',
                'parent_id' => 'nullable|exists:categories,id',
            ]);

            // Create a new Category instance
            $category = new Category();
            $category->category_name = $request->category_name;
            $category->parent_id = $request->parent_id;
            $category->save();
            // Save the image associated with the category

            if ($request->has('image')) {
                $image = save_image('image', 'App\Models\Category', $category->id, $request);
            }

            // Redirect with success message
            return redirect()->route('back.allcategory', compact('category'))->with('success', 'Category created successfully');
        } catch (\Throwable $th) {
            // Redirect with error message
            return redirect()->route('back.allcategory')->with(['error' => 'such as error!']);
        }
    }

    // Display the form to edit a category
    public function editcategory($id){
        // Find the category by ID
        $category_info = Category::findOrFail($id);
        return view('back.editcategory', compact('category_info'));
    }

    // Update an existing category in the database
    public function updatecategory(Request $request, $id) {
        // Validate the incoming request data
        $data = $this->validate($request, [
            'category_name' => 'required|string|max:255',
            'image' => 'sometimes|image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);
        // Find the category by ID
        $category_info = Category::findOrFail($id);
        // Update category information
        $category_info->category_name = $request->category_name;
        // Check if a new image is provided
        if ($request->has('image')) {
            // Delete the old image and its record
            if (File::exists($category_info->images->image ?? '')) {
                File::delete($category_info->images->image);
                Image::where(['id' => $category_info->images->id])->delete();
            }
            // Save the new image
            save_image('image', 'App\Models\Category', $category_info->id, $request);
        }
        // Save the updated category
        $category_info->save();
        // Redirect with success or error message
        if ($category_info) {
            return redirect()->route('back.allcategory')->with(['success' => 'Category Update Successfully!']);
        } else {
            return redirect()->route('back.allcategory')->with(['error' => 'such as error!']);
        }
    }

    // Delete a category from the database
    public function deletecategory($id){
        // Delete the category by ID
        $category_info = Category::where(['id' => $id])->delete();
        // Redirect with success message
        if ($category_info) {
            return redirect()->route('back.allcategory')->with(['success' => 'Delete Successfully']);
        }
    }
}
