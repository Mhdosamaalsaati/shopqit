<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Image;
use App\Models\Product;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    // Constructor with middleware to check user permissions
    public function __construct(){
        $this->middleware('can:product');
    }

    // Display all products
    public function index(){
    // Get all products ordered by the latest
        $allproduct = Product::latest()->get();
        return view('back.allproduct', compact('allproduct'));
    }

    // Display the form to add a new product
    public function Addproduct(){
    // Get all products and categories from the model
        $product_info = Product::all();
        $categories = Category::get();
        return view('back.addproduct',compact('product_info','categories'));
    }

    // Store a new product in the database
    public function storeproduct(Request $request){
        try {
    // Validate the incoming request
            $this->validate($request, [
                'image' => 'required',
                'product_name' => 'required',
                'product_des' => 'required',
                'price' => 'required',
                'category_id' => 'required',
            ]);
    // Create a new Product instance
            $product_info = new Product();
            $product_info->product_name = $request->product_name;
            $product_info->product_des = $request->product_des;
            $product_info->price = $request->price;
            $product_info->category_id = $request->category_id;
            $product_info->save();
    // Save the image associated with the product
            if ($request->has('image')) {
                $image = save_image('image', 'App\Models\Product', $product_info->id, $request);
            }
    // Redirect with success message
            return redirect()->route('back.allproduct', compact('product'))->with(['success', 'Product created successfully']);
        } catch (\Throwable $th) {
    // Redirect with error message
            return redirect()->route('back.allproduct')->with(['error' => 'such as error!']);}
    }

    // Display the form to edit a product
    public function editproduct($id){
    // Find the product by ID with its associated images and categories
        $product_info = Product::with('images')->where(['id' => $id])->first();
        $categories = Category::get();
        return view('back.editproduct', compact('product_info','categories'));
    }

    // Update an existing product in the database
    public function updateproduct(Request $request , $id){
    // Validate the incoming request data
        $data = $this->validate($request, [
            'product_name' => 'required',
            'product_des' => 'required',
            'price' => 'required',
            'category_id' => 'required',
        ]);

        // Find the product by ID
        $product_info = Product::findOrFail($id);
        // Update product information
        $product_info->category_id = $request->category_id ?? $product_info->category_id;
        $product_info->product_name = $request->product_name ?? $product_info->product_name;
        $product_info->product_des = $request->product_des ?? $product_info->product_des;
        $product_info->price = $request->price ?? $product_info->price;
                // Delete the old image and its record
        if (File::exists($product_info->images->image ?? '')) {
            File::delete($product_info->images->image);
            Image::where(['id' => $product_info->images->id])->delete();
        }
                // Save the new image
        save_image('image', 'App\Models\Product', $product_info->id, $request);
                // Save the updated product
        $product_info->save();
                // Redirect with success or error message
        if ($product_info) {
            return redirect()->route('back.allproduct')->with(['success'=>'Product Update Successfully!']);
        } else {
            return redirect()->route('back.allproduct')->with(['error' => 'such as error!']);
        }
    }

    // Delete a product from the database
    public function deleteproduct($id){
                // Delete the product by I
        $product_info = Product::where(['id' => $id])->delete();
        // Redirect with success message
        if ($product_info) {
            return redirect()->route('back.allproduct')->with(['success' => ' Product Delete Successfully']);
        }
    }
}
