<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolesController extends Controller
{

    public function __construct()
    {
        $this->middleware('can:role');
    }

    public function index()
    {
        $roles = Role::get();
        $permissions = Permission::get();
        return view('back.roles.index', compact('roles', 'permissions'));
    }

    public function create()
    {
        $permissions = Permission::get();
        return view('back.roles.create', compact('permissions'));
    }

    public function store(Request $request)
    {
        try {
            $data = $this->validate($request, [
                'role' => 'required',
                'permission' => 'required',
            ]);
            $checked = false;
            $role = new Role();
            $role->name = $data['role'];
            $role->guard_name = 'web';
            $role->save();

            foreach ($request->permission as $key => $permission) {
                $role->givePermissionTo($permission);
                $checked = true;
            }

            if ($checked) {
                return back()->with(['success' => 'Added Role With Permission Successfully']);
            } else {
                return back()->with(['error' => 'Such as Error!']);
            }
        } catch (\Throwable $th) {
            return back()->with(['error' => $th->getMessage()]);
        }
    }
}
